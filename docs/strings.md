## Functions:

### format(value: string, ...args: string[]): string
[//]: # (Why we need this function?)
    Description:
        Give the ability to pass args to function and format them into string. 

    Example:

        let value = format('We are the {0}', ['champions'])

        value -> 'We are the champions'

### replaceAll(string: string, pattern: string, replacement: string): string

    Description:
        replaceAll in string because this function not exists on old node versions.

    Example:

        let value = replaceAll('To be or not to be', 'be', 'do')

        value -> 'To do or not to do'

### reverse(string: string): string
[//]: # (Do we need this function?)
    Description:
        Just reverse strings.

    Example:

        let value = reverse('Pulchritudinous')
        value -> 'suonidutirhcluP'
    
### trimEnd(string: string, pattren: string = ' '): string

    Description:
        Remove spaces from the end of string. 

    Example:

        let value = trimEnd('What?    ')

        value -> 'What?'

### capitalizeFirstLetter(string: string): string

    Description:
        As the name say. 

    Example:

        let value = capitalizeFirstLetter('ke?')

        value -> 'Ke?'

### stringsThatContains(strings: string[], term: string): string[]

    Description:
        Receive array of strings and return array of string that contains the term. 

    Example:

        let value = stringsThatContains(['Why and What', 'The first in teh world', 'and in the universe', 'and'], 'and')

        value -> ['Why and What', 'and in the universe', 'and']