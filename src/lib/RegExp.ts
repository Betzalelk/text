import * as vm from 'vm';

export const regExpTimeout: number = parseInt(process.env.AV_REGEXP_TIMEOUT) | 1000;

let script: vm.Script;

export function exec(string: string, pattern: string, flags?: string): string[] {
    if (!script)
        script = new vm.Script('(new RegExp(global.RegExpPatternParam, global.RegExpFlagsParam)).exec(global.RegExpStringParam);');

    let globalAny = (global as any);
    globalAny.RegExpStringParam = string;
    globalAny.RegExpPatternParam = pattern;
    globalAny.RegExpFlagsParam = flags;

    try {
        return script.runInThisContext({ timeout: regExpTimeout });
    } catch (e) {
        console.error("Error at regExp parsing:", e);
    }

    return null;
}