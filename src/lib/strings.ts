export function format(value: string, ...args: string[]): string {
    let count: number = 0;
    args.forEach(function (item, index) {
        let indexer: string = "{" + index + "}";
        value = replaceAll(value, indexer, item);
        count++;
    });
    return value;
};

export function replaceAll(string: string, pattern: string, replacement: string): string {
    return string.replace(new RegExp(pattern.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), replacement && replacement.toString().split("$").join("$$"));
}

export function reverse(string: string): string {
    return string.split("").reverse().join("");
}

export function trimEnd(string: string, pattren: string = ' '): string {
    while (string.endsWith(pattren)) {
        string = string.slice(0, -1);
    }

    return string;
}

export function capitalizeFirstLetter(string: string): string {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function stringsThatContains(strings: string[], term: string): string[] {
    let results: string[] = [];
    for (let i = 0; i < strings.length; i++)
        if (strings[i].indexOf(term) > -1)
            results.push(strings[i]);

    return results;
}