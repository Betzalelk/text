import test from "ava";
import {reverse, replaceAll} from "../lib/strings";

test("replaceAll 1", t => {
    t.is(replaceAll('foo', 'o', 'a'), 'faa');
})

test("replaceAll 2", t => {
    t.is(replaceAll('To be or not to be', 'be', 'do'), 'To do or not to do');
})

test("replaceAll 3", t => {
    t.is(replaceAll('To be or not to be', 'to', 'H'), 'To be or not H be');
})

test("reverse 1", t => {
    t.is(reverse('foo'), 'oof');
})

test("reverse 2", t => {
    t.is(reverse('Pulchritudinous'), 'suonidutirhcluP');
})

test("reverse 3", t => {
    t.is(reverse('PhP'), 'PhP');
})