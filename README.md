# Text utilities #



### This repository for text helpers functions ###

* Version 0.0.1
* [documentation](https://bitbucket.org/Betzalelk/text/src/master/docs/strings.md)

## Usage

To install text, run:

```console
npm install @reflectiz/text
```
```js
import * as text from '@reflectiz/text'

let reverseString = text.reverse('foo');
```

### Contribution guidelines ###

* Before merge run:
    ```console
    npm test
    ```
    Make sure that all test pass.

* Writing tests
    Make sure that the tests are respectively to code. add test if needed.
* Documentation
    Add or edit docs respectively to code changes.
* TODO
    - Add test for all functions

